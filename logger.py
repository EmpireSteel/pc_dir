# coding=utf-8
import os
import datetime


filename = str(datetime.datetime.now())[:10] + '.log'
path = '/etc/zabbix/script/'
file = path + 'logs/' + filename
try:
    os.mkdir(path + 'logs/')
except:
    pass
if filename not in os.listdir(path + 'logs/'):
    for i in os.listdir(path + 'logs/'):
        os.remove(path + 'logs/' + i)
else:
    if os.stat(file).st_size == 0:
        os.remove(file)
windows = os.popen('/bin/bash ' + path + 'get_window.sh').read()
match = False
window = windows.split('\n')[4].split('"')[1:-1]
window = "'".join(window)
if window == "":
    window = windows.split('\n')[1].split('"')[1:-1]
    window = "'".join(window)
    if window == '':
        window = 'no named window'
check = list(window)
for i in range(len(check)):
    if check[i] == ' ':
        check[i] = '_'
    elif check[i] == "'":
        check[i] = ''
window = ''.join(check)
try:
    with open(file, 'r') as f:
        log = f.readlines()
    with open(file, 'w') as f:
        for i in log:
            i = i.split('\t')
            if i[0] == window:
                match = True
                i[1] = str(int(i[1]) + 60) + '\n'
            f.write('\t'.join(i))
        if not match:
            f.write(window + '\t60\n')
except IOError:
    with open(file, 'w') as f:
        print window
        f.write(window + '\t60\n')
