# coding=utf-8
import os
import datetime

path = '/etc/zabbix/script/'
date = str(datetime.datetime.now())[:10]
windows = os.popen('cat ' + path + 'logs/' + date + '.log | cut -f1').read()
windows = windows.split('\n')
json = '{"data":['
sep = ''
for win in windows:
    if win != '':
        json += sep + '{"{#WINDOW}":"' + win + '"}'
        sep = ','
json += ']}'
print json
