# coding=utf-8
import argparse
import datetime


def get_value(window, log):
    with open(log, 'r') as f:
        for i in f.readlines():
            line = i.split()
            if window == line[0]:
                return int(line[1])
    return 0


if __name__ == '__main__':
    path = ''
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--window')
    args = parser.parse_args()
    window = args.window
    log = path + 'logs/' + str(datetime.datetime.now())[:10] + '.log'
    print get_value(window, log)